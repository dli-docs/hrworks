:page-partial:
:imagesdir: ../../assets/images

If you have multiple companies and want to control access for the user/ roles to these companies, do any of the following.

[cols="25,75",options="header"]
|===
|IF
|THEN

|To grant access to all companies in the HRW database.
a|Select the *All Company Access Rights* check box.

NOTE: By doing this, the user or the user who has been assigned with the respective role can log into the all companies in the database.

|To grant access to one or more individual companies.
a|Do the following:

1. Unselect the *All Company Access Rights* check box.

2. On the *Companies* tab that appears, select the companies to which you want to grant access for the role/ user.
|===