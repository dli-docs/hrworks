:page-partial:
:imagesdir: ../../assets/images

To sets the program access rights, follow the steps below.

. Do any of the following as required
+
IMPORTANT: This step is relevant only if you are setting permission for a user. If you are setting permission for roles, skip this step and proceed with the *Step 2*.
+
* *To set permission at the user-level*:
+
.. From *Program Access Rights* dropdown field, select the __User Linked__ option.
.. To set permission at the user-level, proceed with *Step 2*.
+
* *To set permission at the role-level*:
+
.. From *Program Access Rights* dropdown field, select the __Role Linked__ option.
.. From the *Role Access Rights* tab that appears at the bottom pane, select the role/s to which you want to assign the user.
+
After you save the changes, the user will inherit the permissions set assigned role assigned to him/ her.

* *To set permission at the both user-level and role-level*:
.. From *Program Access Rights* dropdown field, select the __User + Role__ option.
.. From the *Role Access Rights* tab that appears at the bottom pane, select the role/s to which you want to assign the user.
+
After you save the changes, the user will inherit the permissions set assigned role assigned to him/ her and any permission set for him/ her at the user-level.
.. To set permission at the user-level, proceed with *Step 2*.

. Do any of the following as required.
+
[cols="25,75",options="header"]
|===
|IF
|THEN

a|To provide roles or users with the following permissions to all programs.

* Add, Modify, Delete, Approve, and/or Inquiry/ print.

a|Do the following.

. From the *All Programs Access Rights* drop-down field, make appropriate selection.

. Click *OK*.

a|To provide roles or users with the following permissions to one or more programs individually.

* Add, Modify, Delete, Approve, and/or Inquiry/ print.

a|Do the following:

. Unselect the *Programs Access Rights* check box selections.

. On the *Programs Access Rights* tab that appears, make appropriate selections against each program.

|===