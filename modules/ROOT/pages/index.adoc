= HR Works Intro
Adarsh <adarsh@dli-pdc.com>
//Settings
:description: Introduction to HR Works.
:toc: preamble
:imagesdir: ../assets/images
:v_core: v3
:experimental:
:icons: font

HR Works is a feature-rich HR and Payroll software that meets the needs of organizations of any size. It has well-integrated and customizable modules for managing payroll, attendance, leaves, indemnities and more.

HR Works supports multiple-currency and multi-location payroll systems. Being a parameter- driven software, it allows you to set up and configure custom data fields, policies, and parameters that are specific to your company. Users can also generate and print various customizable reports.